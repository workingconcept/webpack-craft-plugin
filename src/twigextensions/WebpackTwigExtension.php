<?php
/**
 * Webpack plugin for Craft CMS 3.x
 *
 * @link      https://workingconcept.com
 * @copyright Copyright (c) 2018 Working Concept
 */

namespace workingconcept\webpack\twigextensions;

use workingconcept\webpack\Webpack;
use workingconcept\webpack\variables\WebpackVariable;

use Craft;
use Twig_Filter;
use Twig_Function;

class WebpackTwigExtension extends \Twig_Extension implements \Twig_Extension_GlobalsInterface
{
    
    public function getName()
    {
        return 'Webpack';
    }

    public function getFilters()
    {
        $filters = [];

        foreach ($this->getMethods() as $method => $args)
        {
            $filters[] = new Twig_Filter('webpack_' . $method, $args, ['is_safe' => ['html']]);
        }

        return $filters;
    }

    public function getFunctions()
    {
        $functions = [];

        foreach ($this->getMethods() as $method => $args)
        {
            $functions[] = new Twig_Function('webpack_' . $method, $args, ['is_safe' => ['html']]);
        }

        return $functions;
    }

    public function getGlobals()
    {
        return ['webpack' => Webpack::$plugin->webpack];
    }

    private function getMethods()
    {
        return [
            'js'         => [Webpack::$plugin->webpack, 'getManifestJs'],
            'js_inline'  => [Webpack::$plugin->webpack, 'getManifestJsInline'],
            'js_tags'    => [Webpack::$plugin->webpack, 'getManifestJsTags'],
            'css'        => [Webpack::$plugin->webpack, 'getManifestCss'],
            'css_inline' => [Webpack::$plugin->webpack, 'getManifestCssInline'],
            'css_tags'   => [Webpack::$plugin->webpack, 'getManifestCssTags']
        ];
    }
}