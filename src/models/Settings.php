<?php

namespace workingconcept\webpack\models;

use Craft;
use craft\base\Model;

class Settings extends Model
{
    // Public Properties
    // =========================================================================

    /**
     * @var string
     */
    public $baseUrl = '';

    /**
     * @var string
     */
    public $manifestPath = '';

    // Public Methods
    // =========================================================================

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['baseUrl', 'manifestPath'], 'string']
        ];
    }
}
