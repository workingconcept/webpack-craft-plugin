<?php
/**
 * Webpack plugin for Craft CMS 3.x
 *
 * @link      https://workingconcept.com
 * @copyright Copyright (c) 2018 Working Concept
 */

namespace workingconcept\webpack\variables;

use workingconcept\webpack\Webpack;

use Craft;

/**
 * @author    Working Concept
 * @package   Webpack
 * @since     1.0.0
 */
class WebpackVariable
{
    /**
     * Returns an array of JavaScript paths from the manifest.
     *
     * @return string
     */
    public function js($filename)
    {
        return Webpack::$plugin->webpack->getManifestJs($filename);
    }

    /**
     * Returns an array of JavaScript tags from the manifest.
     *
     * @return string
     */
    public function jsTags($filename)
    {
        return Webpack::$plugin->webpack->getManifestJsTags($filename);
    }

    /**
     * Returns an array of stylesheet paths from the manifest.
     *
     * @return string
     */
    public function css($filename)
    {
        return Webpack::$plugin->webpack->getManifestCss($filename);
    }

    /**
     * Returns an array of stylesheet tags from the manifest.
     *
     * @return string
     */
    public function cssTags($filename)
    {
        return Webpack::$plugin->webpack->getManifestCssTags($filename);
    }

}
