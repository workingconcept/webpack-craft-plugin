<?php
/**
 * Webpack plugin for Craft CMS 3.x
 *
 * @link      https://workingconcept.com
 * @copyright Copyright (c) 2018 Working Concept
 */

namespace workingconcept\webpack\services;

use workingconcept\webpack\Webpack;

use Craft;
use craft\base\Component;

/**
 * @author    Working Concept
 * @package   Webpack
 * @since     1.0.0
 */
class WebpackService extends Component
{

    /**
     * @var \workingconcept\webpack\models\Settings
     */
    public $settings;

    /**
     * @var boolean
     */
    protected $isConfigured;

    /**
     * Read and decoded contents of JSON manifest.
     *
     * @var array
     */
    protected $manifestData;

    /**
     * Public URL prefix referenced assets.
     *
     * @var string
     */
    protected $publicPath;


    // Public Methods
    // =========================================================================

    /**
     * Initializes the service.
     *
     * @return void
     */
    public function init()
    {
        parent::init();

        $this->settings   = Webpack::$plugin->getSettings();
        $this->publicPath = Craft::getAlias($this->settings->baseUrl);

        $webroot      = Craft::getAlias('@webroot');
        $manifestPath = $webroot . '/' . $this->settings->manifestPath;

        if ( ! file_exists($manifestPath)) 
        {
            throw new \Error("Manifest file doesn't exist: `" . $manifestPath. "`");
        }

        if ( ! $this->manifestData = json_decode(file_get_contents($manifestPath), true))
        {
            throw new \Error("Can't read manifest at `" . $manifestPath. "`");
        }
    }

    /**
     * Get public-facing URL for each JavaScript file referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestJs($filename)
    {
        return $this->getManifestEntries($filename, 'js');
    }

    /**
     * Get public-facing HTML tag for each JavaScript file referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestJsTags($filename)
    {
        return $this->generateHtmlTags($filename, 'js');
    }

    /**
     * Get inline JavaScript for entry filename referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestJsInline($filename)
    {
        return $this->getManifestEntriesInline($filename, 'js');
    }

    /**
     * Get public-facing URL for each CSS file referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestCss($filename)
    {
        return $this->getManifestEntries($filename, 'css');
    }

    /**
     * Get public-facing HTML tag for each CSS file referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestCssTags($filename)
    {
        return $this->generateHtmlTags($filename, 'css');
    }

    /**
     * Get inline CSS for entry filename referenced in the manifest.
     *
     * @param  string $filename  base filename
     * 
     * @return void
     */
    public function getManifestCssInline($filename)
    {
        return $this->getManifestEntriesInline($filename, 'css');
    }

    // Private Methods
    // =========================================================================


    /**
     * Get entries matching the provided base filename and type from parsed manifest.json data.
     *
     * @param string  $filename                Base entry point filename.
     * @param string  $type                    'js' or 'css'
     * @param boolean $skipArrayForSingleItem  Return a single item directly, instead of in an array.
     * 
     * @return mixed  array or string
     */
    private function getManifestEntries($filename, $type, $skipArrayForSingleItem = true)
    {
        $result = [];

        if (isset($this->manifestData[$filename . '.' . $type]))
        {
            $result[] = $this->publicPath . $this->manifestData[$filename . '.' . $type];
        }

        if (count($result) === 1 && $skipArrayForSingleItem)
        {
            return $result[0];
        }

        return $result;
    }

    /**
    * Get inline contents of the matching base filename from the manifest.
    *
    * @param string  $filename                Base entry point filename.
    * @param string  $type                    'js' or 'css'
    * @param boolean $skipArrayForSingleItem  Return a single item directly, instead of in an array.
    * 
    * @return mixed  array or string
    */
    private function getManifestEntriesInline($filename, $type, $skipArrayForSingleItem = true)
    {
        $webroot = Craft::getAlias('@webroot');
        $items   = $this->getManifestEntries($filename, $type, $skipArrayForSingleItem);
        $result  = [];

        if ( ! is_array($items))
        {
            $items = [$items];
        }

        foreach ($items as $item)
        {
            $result[] = file_get_contents($webroot . $item);
        }

        if (count($result) === 1 && $skipArrayForSingleItem)
        {
            return $result[0];
        }

        return $result;
    }

    /**
     * Return JavaScript or CSS tags for entry filename(s).
     *
     * @param string  $filename  Base entry point filename.
     * @param string  $type      'js' or 'css'
     * 
     * @return array
     */
    private function generateHtmlTags($filename, $type)
    {
        $items = $this->getManifestEntries($filename, $type, false);
        $tags  = [];

        foreach ($items as $item)
        {
            if ($type === 'js')
            {
                $tags[] = '<script src="' . $item . '"></script>';
            }
            elseif ($type === 'css')
            {
                $tags[] = '<link rel="stylesheet" href="' . $item . '">';
            }
        }

        return $tags;
    }
}
