<?php
/**
 * Webpack plugin for Craft CMS 3.x
 *
 * @link      https://workingconcept.com
 * @copyright Copyright (c) 2018 Working Concept
 */

namespace workingconcept\webpack;

use workingconcept\webpack\models\Settings;
use workingconcept\webpack\variables\WebpackVariable;
use workingconcept\webpack\services\WebpackService;
use workingconcept\webpack\twigextensions\WebpackTwigExtension;

use Craft;
use craft\base\Plugin;
use craft\services\Plugins;
use craft\events\PluginEvent;
use craft\web\twig\variables\CraftVariable;

use yii\base\Event;

class Webpack extends Plugin
{

    public static $plugin;
    public $schemaVersion = '1.0.0';

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();
        self::$plugin = $this;

        $settings = $this->getSettings();

        $this->setComponents([
            'webpack' => WebpackService::class
        ]);

        if (Craft::$app->request->getIsSiteRequest()) 
        {
            Event::on(
                CraftVariable::class,
                CraftVariable::EVENT_INIT,
                function (Event $event) {
                    /** @var WebpackVariable $variable */
                    $variable = $event->sender;
                    $variable->set('webpack', WebpackVariable::class);
                }
            );
    
            $extension = new WebpackTwigExtension();
            Craft::$app->view->registerTwigExtension($extension);
        }

        if ( ! $settings->validate()) 
        {
            throw new InvalidConfigException('Webpack plugin has invalid settings.');
        }
    }

    /**
     * @inheritdoc
     */
    protected function createSettingsModel()
    {
        return new Settings();
    }

    /**
     * @inheritdoc
     */
    protected function settingsHtml(): string
    {
        return Craft::$app->view->renderTemplate(
            'webpack/settings',
            [
                'settings' => $this->getSettings()
            ]
        );
    }

}
