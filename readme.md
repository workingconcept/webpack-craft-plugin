# 🚨 This plugin is deprecated and you should consider using [Twigpack](https://github.com/nystudio107/craft-twigpack) instead, which is more flexible and supports webpack-dev-server and other fancies. 🚨


## Twig+Webpack Craft Plugin

Parses Webpack's JSON manifest to easily include bundles in your Craft templates. Inspired by [fullpipe/twig-webpack-extension]() and makes filename-only references available in addition to HTML tags.

### Use

1. Set up your workflow to use Webpack and generate manifest.json.
2. Add this plugin via `composer require workingconcept/craft-webpack`, and install from the Craft control panel.
3. Configure the plugin to find your manifest and (optionally) tailor your output URLs.
4. Reference assets in your templates.

### Template Tags

There are a few ways of doing the same thing. You're ultimately providing the base name of your desired Webpack entry, either getting a URL (`/dist/js/bundle.main.js`) or an HTML tag (`<script src="/dist/js/bundle.main.js"></script>`).

If there's only a single entry point, it'll be returned directly. If there are multiple entry points returned, they'll be returned in an array. All `_tags` results, however, will be arrays even if empty.

#### JavaScript

```
{{ webpack_js('main') }}
{{ 'main' | webpack_js }}
```

Will output `/dist/js/main.bundle.js`.

```
{% for tag in 'main' | webpack_js_tags %}
    {{ tag | raw }}
{% endfor %}

{% for tag in webpack_js_tags('main') %}
    {{ tag | raw }}
{% endfor %}
```

Will output `<script src="/dist/js/main.bundle.js"></script>` for each line.

#### CSS

```
{{ webpack_css('main') }}
{{ 'main' | webpack_css }}
```

Will output `/dist/css/main.bundle.css`.

```
{% for tag in 'main' | webpack_css_tags %}
    {{ tag | raw }}
{% endfor %}

{% for tag in webpack_css_tags('main') %}
    {{ tag | raw }}
{% endfor %}
```

Will output `<link rel="stylesheet" href="/dist/css/main.bundle.css">` for each line.


---

### TODO:

- Allow customization of tag HTML.